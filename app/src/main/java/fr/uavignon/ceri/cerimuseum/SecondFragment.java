package fr.uavignon.ceri.cerimuseum;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;

import fr.uavignon.ceri.cerimuseum.data.Item;

public class SecondFragment extends Fragment {

    private SecondViewModel secondViewModel;
    private TextView name, categories, timeFrame, description, year, brand, technicalDetails, isWorking;
    private ViewPager2 viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private CardView cardView;
    private Button likeBtn;
    private String id;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        secondViewModel = new ViewModelProvider(this).get(SecondViewModel.class);
        viewPager = (ViewPager2) view.findViewById(R.id.viewPager2);

        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
        id = args.getId();

        secondViewModel.setItem(id);

        name = view.findViewById(R.id.name);
        description = view.findViewById(R.id.descritpion);
        categories = view.findViewById(R.id.categories);
        timeFrame = view.findViewById(R.id.timeFrame);
        year = view.findViewById(R.id.year);
        brand = view.findViewById(R.id.brand);
        technicalDetails = view.findViewById(R.id.technicalDetails);
        isWorking = view.findViewById(R.id.isWorking);
        likeBtn = view.findViewById(R.id.likeBtn);



        observerSetup();
        listenerSetup();
    }

    private void listenerSetup() {
        secondViewModel.getAllItemPictures().observe(getViewLifecycleOwner(), allItemPictures ->{
            viewPagerAdapter =  new ViewPagerAdapter();
            viewPager.setAdapter(viewPagerAdapter);
            viewPagerAdapter.setItemPictures(allItemPictures);
            if(allItemPictures.size() == 0){
                cardView = getView().findViewById(R.id.cardView3);
                cardView.removeAllViews();
            }
        });

        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(id);
                if(likeBtn.getText() == "Like") {
                    likeBtn.setText("Dislike");
                    secondViewModel.likeItem(id);
                }
                else if(likeBtn.getText() == "Dislike"){
                    likeBtn.setText("Like");
                    secondViewModel.dislikeItem(id);
                }
            }
        });
    }

    private void observerSetup() {
        secondViewModel.getItem().observe(getViewLifecycleOwner(), item -> {
            name.setText(item.getName());
            description.setText(item.getDescription());
            categories.setText(item.getCategories());
            timeFrame.setText(item.getTimeFrame());
            if(item.getYear() == 0)
                year.setText("N/A");
            else
                year.setText(item.getYear()+"");

            if(item.getBrand() == null)
                brand.setText("N/A");
            else
                brand.setText(item.getBrand());
            if(item.getTechnicalDetails() == null)
                technicalDetails.setText("N/A");
            else
                technicalDetails.setText(item.getTechnicalDetails());

            if (item.isWorking())
                isWorking.setText("Working");
            else
                isWorking.setText("Not Working");

            if (item.isLiked())
                likeBtn.setText("Dislike");
            else
                likeBtn.setText("Like");
        });


    }
}