package fr.uavignon.ceri.cerimuseum;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.ItemPictures;
import fr.uavignon.ceri.cerimuseum.data.MuseumRepository;

public class SecondViewModel extends AndroidViewModel {

    private MuseumRepository museumRepository;
    private MutableLiveData<Item> item;
    private LiveData<List<ItemPictures>> allItemPictures;

    public SecondViewModel(@NonNull Application application) {
        super(application);
        museumRepository = MuseumRepository.get(application);

    }

    public void setItem(String id){
        museumRepository.loadItem(id);
        museumRepository.loadAllItemPictures(id);
        item = museumRepository.getSelectedItem();
        allItemPictures = museumRepository.getAllItemPictures();
    }

    public LiveData<Item> getItem(){
        return item;
    }

    public LiveData<List<ItemPictures>> getAllItemPictures(){
        return allItemPictures;
    }

    public void likeItem(String id){
        museumRepository.likeItem(id);
    }

    public void dislikeItem(String id){
        museumRepository.dislikeItem(id);
    }
}
