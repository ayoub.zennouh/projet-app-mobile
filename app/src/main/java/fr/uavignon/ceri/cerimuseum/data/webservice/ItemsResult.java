package fr.uavignon.ceri.cerimuseum.data.webservice;

import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.cerimuseum.data.FavoriteItems;
import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.ItemCategories;
import fr.uavignon.ceri.cerimuseum.data.ItemPictures;
import fr.uavignon.ceri.cerimuseum.data.database.MuseumDao;

public class ItemsResult {

    public static void transferInfo(Map<String, ItemsResponse> responseMap, MuseumDao museumDao, Item item, ItemPictures itemPictures, ItemCategories itemCategories, List<String> favoriteItemsList){
        for (Map.Entry<String,ItemsResponse> entry : responseMap.entrySet()){

            if(favoriteItemsList != null && favoriteItemsList.contains(entry.getKey()))
                item.setLiked(true);
            else
                item.setLiked(false);

            ItemsResponse responseItem = entry.getValue();

            if(responseItem.pictures != null) {
                for (Map.Entry<String, String> picture : responseItem.pictures.entrySet()) {
                    itemPictures.setItemId(entry.getKey());
                    itemPictures.setPicture(picture.getKey());
                    itemPictures.setCaption(picture.getValue());
                    museumDao.insertPicture(itemPictures);
                }
            }

            for (String category: responseItem.categories){
                itemCategories.setItemId(entry.getKey());
                itemCategories.setCategory(category);
                museumDao.insertCategory(itemCategories);
            }

            item.setId(entry.getKey());
            item.setName(responseItem.name);
            item.setDescription(responseItem.description);
            item.setCategories(responseItem.categories.toString().replace("[","").replace("]",""));
            item.setTimeFrame(responseItem.timeFrame.toString().replace("[","").replace("]",""));
            item.setBrand(responseItem.brand);
            item.setYear(responseItem.year);
            if(responseItem.technicalDetails != null)
                item.setTechnicalDetails(responseItem.technicalDetails.toString().replace("[","").replace("]",""));
            else
                item.setTechnicalDetails(null);
            item.setWorking(responseItem.working);
            museumDao.insert(item);
        }
    }
}
