package fr.uavignon.ceri.cerimuseum;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Category;

public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Category> categoriesList;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categories_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.categories.setText(categoriesList.get(position).getCategory());
        viewHolder.count.setText(categoriesList.get(position).getCount());
    }


    public void setCategoriesList(List<Category> categoriesList){
        this.categoriesList = categoriesList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return categoriesList == null ? 0: categoriesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView categories;
        TextView count;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            categories = itemView.findViewById(R.id.category_name);
            count = itemView.findViewById(R.id.count);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String category = CategoriesRecyclerAdapter.this.categoriesList.get((int) getAdapterPosition()).getCategory();
                    CategoriesFragmentDirections.ActionCategoriesFragmentToListFragment action = CategoriesFragmentDirections.actionCategoriesFragmentToListFragment();
                    action.setCategory(category);
                    Navigation.findNavController(view).navigate(action);
                }
            });
        }
    }
}
