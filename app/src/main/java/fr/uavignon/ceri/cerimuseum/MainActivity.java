package fr.uavignon.ceri.cerimuseum;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;

public class MainActivity extends AppCompatActivity {

    private MainActivity mainActivity = this;
    private TabHost tabs;
    private SearchView searchView;
    private ListViewModel listViewModel;
    private List<Item> items;
    private int counterAlph;
    private int counterYear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);

        counterAlph = 0;
        counterYear = 0;

        tabs = (TabHost) findViewById(R.id.tabs);
        tabs.setup();
        TabHost.TabSpec spec = tabs.newTabSpec("1");
        spec.setContent(R.id.Items);
        spec.setIndicator("Items");
        tabs.addTab(spec);
        spec = tabs.newTabSpec("2");
        spec.setContent(R.id.Categories);
        spec.setIndicator("Categories");
        tabs.addTab(spec);

        searchView = findViewById(R.id.searchView);

        listenerSetup();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        String label = (String) Navigation.findNavController(mainActivity, R.id.nav_host_fragment).getCurrentDestination().getLabel();
        super.onBackPressed();
        if(label.equals("Categories Fragment"))
            tabs.setCurrentTabByTag("1");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
            if (id == R.id.by_year) {
                if (counterYear%2 == 0)
                    listViewModel.getCollectionByYear();
                else
                    listViewModel.getCollectionByYearDesc();
                counterYear++;
                return true;
            }
            if(id == R.id.by_alph){
                if(counterAlph%2 == 0)
                    listViewModel.getCollectionByAlph();
                else
                    listViewModel.getCollectionByAlphAsc();
                counterAlph++;
                return true;
            }

        return super.onOptionsItemSelected(item);
    }

    private void listenerSetup(){


        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listViewModel.getCollectionByAlphAsc();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            ArrayList<Item> itemsList;
            @Override
            public boolean onQueryTextSubmit(String query) {
                items = listViewModel.getItems().getValue();
                itemsList = new ArrayList<Item>();
                for(Item item: items){
                    if(item.getName().contains(query) || item.getCategories().contains(query) || item.getDescription().contains(query)){
                        itemsList.add(item);
                    }
                }
                listViewModel.setListItems(itemsList);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                items = listViewModel.getItems().getValue();
                itemsList = new ArrayList<Item>();
                for(Item item: items){
                    if(item.getName().contains(newText) || item.getCategories().contains(newText) || item.getDescription().contains(newText)){
                        itemsList.add(item);
                    }
                }
                listViewModel.setListItems(itemsList);
                return false;
            }
        });

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                NavController navController = Navigation.findNavController(mainActivity, R.id.nav_host_fragment);
                String label = (String) navController.getCurrentDestination().getLabel();
                if (s == "1"){
                    if(label.equals("Categories Fragment")) {
                        CategoriesFragmentDirections.ActionCategoriesFragmentToListFragment action = CategoriesFragmentDirections.actionCategoriesFragmentToListFragment();
                        navController.navigate(action);
                    }else if(label.equals("Detail Fragment")){
                        SecondFragmentDirections.ActionDetailFragmentToListFragment action = SecondFragmentDirections.actionDetailFragmentToListFragment();
                        navController.navigate(action);
                    }else if(label.equals("List Fragment")){
                        ListFragmentDirections.ActionListFragmentSelf2 action = ListFragmentDirections.actionListFragmentSelf2();
                        navController.navigate(action);
                    }

                }else{
                    if(label.equals("List Fragment")) {
                        ListFragmentDirections.ActionListFragmentToCategoriesFragment action = ListFragmentDirections.actionListFragmentToCategoriesFragment();
                        navController.navigate(action);
                    }else if(label.equals("Detail Fragment")){
                        SecondFragmentDirections.ActionDetailFragmentToCategoriesFragment action = SecondFragmentDirections.actionDetailFragmentToCategoriesFragment();
                        navController.navigate(action);
                    }
                }
            }
        });

    }
}