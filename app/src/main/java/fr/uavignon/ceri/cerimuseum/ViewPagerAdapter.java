package fr.uavignon.ceri.cerimuseum;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.ItemPictures;

public class ViewPagerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.cerimuseum.ViewPagerAdapter.ViewHolder> {

    private List<ItemPictures> itemPictures;


    public void setItemPictures(List<ItemPictures> itemPictures){
        this.itemPictures = itemPictures;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide.with(holder.imageElement.getContext()).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+itemPictures.get(position).getItemId()+"/images/"+itemPictures.get(position).getPicture()).into(holder.imageElement);
        holder.caption.setText(itemPictures.get(position).getCaption());
    }

    @Override
    public int getItemCount() {
        return itemPictures == null ? 0 : itemPictures.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageElement;
        TextView caption;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageElement = itemView.findViewById(R.id.imageView);
            caption = itemView.findViewById(R.id.caption);
        }
    }
}
