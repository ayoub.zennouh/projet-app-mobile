package fr.uavignon.ceri.cerimuseum;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.cerimuseum.data.CollectionVersion;
import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.MuseumRepository;
import fr.uavignon.ceri.cerimuseum.data.webservice.CMInterface;
import fr.uavignon.ceri.cerimuseum.data.webservice.ItemsResponse;

public class ListViewModel extends AndroidViewModel {

    private MuseumRepository repository;

    public ListViewModel(@NonNull Application application) {
        super(application);
        repository = MuseumRepository.get(application);
    }

    public void getCollection(){
        repository.loadCollection();
    }
    public void getCollectionByYear() {
        repository.loadCollectionByYear();
    }
    public void getCollectionByYearDesc(){ repository.loadCollectionByYearDesc();}
    public void getCollectionByAlph() {
        repository.loadCollectionByAlph();
    }
    public void getCollectionByAlphAsc(){
        repository.loadCollectionByAlphAsc();
    }

    public LiveData<List<Item>> getItems(){
        return repository.getItems();
    }

    public MutableLiveData<List<Item>> getMutableItemList(){
        return repository.getMutableItemList();
    }

    public LiveData<CollectionVersion> getCollectionVersion() {
        return repository.getCollectionVersion();
    }

    public void checkVersion(){
        repository.checkVersion();
    }

    public void getItemsByCategory(String category){
        repository.getItemsByCategory(category);
    }

    public void setListItems(List<Item> listItems){
        repository.setItems(listItems);
    }

}
