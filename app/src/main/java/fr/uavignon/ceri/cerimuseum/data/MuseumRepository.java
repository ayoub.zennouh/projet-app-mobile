package fr.uavignon.ceri.cerimuseum.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.cerimuseum.data.database.MuseumDao;
import fr.uavignon.ceri.cerimuseum.data.database.MuseumRoomDatabase;
import fr.uavignon.ceri.cerimuseum.data.webservice.CMInterface;
import fr.uavignon.ceri.cerimuseum.data.webservice.ItemsResponse;
import fr.uavignon.ceri.cerimuseum.data.webservice.ItemsResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.cerimuseum.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {

    private final CMInterface api;
    private MuseumDao museumDao;
    private Application application;

    private LiveData<List<Item>> items;
    private MutableLiveData<List<Item>> mutableItemList;

    private LiveData<List<ItemPictures>> allItemPictures;
    private LiveData<List<Category>> allCategories;
    private LiveData<CollectionVersion> collectionVersion;
    private MutableLiveData<Item> selectedItem;
    private List<String> favoriteItemsList;



    public MutableLiveData<List<Item>> getMutableItemList() {
        return mutableItemList;
    }

    public LiveData<List<Item>> getItems() {
        return items;
    }

    public LiveData<List<ItemPictures>> getAllItemPictures(){
        return allItemPictures;
    }

    public LiveData<List<Category>> getAllCategories() {
        return allCategories;
    }

    public void setItems(List<Item> items) {
        mutableItemList.postValue(items);
    }


    private static volatile MuseumRepository INSTANCE;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    private MuseumRepository(Application application) {
        this.application = application;
        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(CMInterface.class);
        museumDao = db.museumDao();


        mutableItemList = new MutableLiveData<>();
        selectedItem = new MutableLiveData<Item>();

        collectionVersion = museumDao.getLastVersion();
        favoriteItemsList = museumDao.getAllFavorites();
    }

    public void loadItem(String id){
        Future<Item> fitem = databaseWriteExecutor.submit(() ->{
            return museumDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadCollection(){
        Future<LiveData<List<Item>>> liveDataFuture = databaseWriteExecutor.submit(()->{
            return museumDao.getAllItems();
        });
        try {
            items = liveDataFuture.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadCollectionByYear() { mutableItemList.postValue(museumDao.getAllItemsByYear()); }
    public void loadCollectionByYearDesc() { mutableItemList.postValue(museumDao.getAllItemsByYearDesc()); }
    public void loadCollectionByAlph() {
        mutableItemList.postValue(museumDao.getSynchrAllItems());
    }
    public void loadCollectionByAlphAsc() { mutableItemList.postValue(museumDao.getSynchrAllItemsASC());}

    public void loadAllItemPictures(String itemId){
        Future<LiveData<List<ItemPictures>>> liveDataFuture = databaseWriteExecutor.submit(()->{
            return museumDao.getAllPicturesByItemId(itemId);
        });
        try {
            allItemPictures = liveDataFuture.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void updateCollection(){
        Item item = new Item();
        ItemCategories itemCategories = new ItemCategories();
        ItemPictures itemPictures = new ItemPictures();
        api.getCollection().enqueue(new Callback<Map<String, ItemsResponse>>() {
            @Override
            public void onResponse(Call<Map<String, ItemsResponse>> call, Response<Map<String, ItemsResponse>> response) {
                ItemsResult.transferInfo(response.body(), museumDao, item, itemPictures, itemCategories, favoriteItemsList);
            }

            @Override
            public void onFailure(Call<Map<String, ItemsResponse>> call, Throwable t) {
            }
        });
    }

    public void checkVersion(){
        api.getCollectionVersion().enqueue(new Callback<Float>() {
            @Override
            public void onResponse(Call<Float> call, Response<Float> response) {
                if(collectionVersion.getValue() == null || collectionVersion.getValue().getVersion() != response.body()){
                    museumDao.insertVersion(new CollectionVersion(response.body()));
                    museumDao.deleteAll();
                    updateCollection();
                    mutableItemList.postValue(museumDao.getSynchrAllItems());
                }
            }

            @Override
            public void onFailure(Call<Float> call, Throwable t) {
            }
        });
    }

    public void loadCategories(){
        Future<LiveData<List<Category>>>liveDataFuture = databaseWriteExecutor.submit(()->{
            return museumDao.getAllCategories();
        });

        try {
            allCategories = liveDataFuture.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LiveData<CollectionVersion> getCollectionVersion() {
        return collectionVersion;
    }

    public MutableLiveData<Item> getSelectedItem(){
        return selectedItem;
    }

    public void getItemsByCategory(String category){
        Future<List<String>> listFuture = databaseWriteExecutor.submit(()->{
            return museumDao.getItemsIdByCategory(category);
        });

        Future<LiveData<List<Item>>> liveDataFuture = databaseWriteExecutor.submit(()->{
           return museumDao.getItemsByCategory(listFuture.get());
        });

        try {
            items = liveDataFuture.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void likeItem(String id){
        databaseWriteExecutor.submit(()->{
            museumDao.addLike(new FavoriteItems(id));
            museumDao.updateLike(id, true);
        });
        api.likeItem(id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    public void dislikeItem(String id){
        databaseWriteExecutor.submit(()->{
            museumDao.deleteLike(id);
            museumDao.updateLike(id, false);
        });
        api.dislikeItem(id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}
