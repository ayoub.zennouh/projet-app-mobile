package fr.uavignon.ceri.cerimuseum;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;

public class ListFragment extends Fragment {

    private ListViewModel viewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        ListFragmentArgs args = ListFragmentArgs.fromBundle(getArguments());
        String category = args.getCategory();

        if(!category.isEmpty())
            viewModel.getItemsByCategory(category);
        else
            viewModel.getCollection();

        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);

        listenerSetup();
        observerSetup();
    }

    private void observerSetup() {
        viewModel.getItems().observe(getViewLifecycleOwner(), items -> {

            adapter.setItemsList(sortByFavorite(items));
        });

        viewModel.getCollectionVersion().observe(getViewLifecycleOwner(), collectionVersion -> {
            viewModel.checkVersion();
        });

        viewModel.getMutableItemList().observe(getViewLifecycleOwner(), items ->{
            adapter.setItemsList(sortByFavorite(items));
        });
    }

    private void listenerSetup() {

    }

    private List<Item> sortByFavorite(List<Item> items){
        ArrayList<Item> itemsList = new ArrayList<Item>();

        for(Item item:items){
            if (item.isLiked())
                itemsList.add(item);
        }
        for(Item item:items){
            if (!item.isLiked())
                itemsList.add(item);
        }
        return  itemsList;
    }
}