package fr.uavignon.ceri.cerimuseum.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.cerimuseum.data.CollectionVersion;
import fr.uavignon.ceri.cerimuseum.data.FavoriteItems;
import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.ItemCategories;
import fr.uavignon.ceri.cerimuseum.data.ItemPictures;

@Database(entities = {Item.class, CollectionVersion.class, ItemPictures.class, ItemCategories.class, FavoriteItems.class}, version = 1, exportSchema = false)
public abstract class MuseumRoomDatabase extends RoomDatabase {

    public abstract MuseumDao museumDao();

    private static MuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static MuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseumRoomDatabase.class,"museum_database").allowMainThreadQueries()
                                    .build();

                }
            }
        }
        return INSTANCE;
    }
}
