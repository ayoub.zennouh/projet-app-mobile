package fr.uavignon.ceri.cerimuseum.data;

import androidx.room.ColumnInfo;

public class Category {

    public String getCategory() {
        return category;
    }

    public String getCount() {
        return count;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCount(String count) {
        this.count = count;
    }

    private String category;

    @ColumnInfo(name = "count(*)")
    private String count;
}
