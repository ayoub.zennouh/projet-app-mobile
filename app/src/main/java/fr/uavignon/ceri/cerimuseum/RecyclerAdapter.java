package fr.uavignon.ceri.cerimuseum;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public void setItemsList(List<Item> itemsList) {
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    private List<Item> itemsList;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        if(i == 2){
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout1, viewGroup, false);
        }else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.itemTitle.setText(itemsList.get(i).getName());
            viewHolder.itemDetail.setText(itemsList.get(i).getCategories().replace("[", "").replace("]", ""));
            Glide.with(viewHolder.thumbnail.getContext()).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + itemsList.get(i).getId() + "/thumbnail").into(viewHolder.thumbnail);
            if(itemsList.get(i).isLiked()) {
                viewHolder.star.setImageDrawable(viewHolder.star.getResources().getDrawable(viewHolder.star.getResources().getIdentifier("drawable/star",
                        null, viewHolder.star.getContext().getPackageName())));
            }else{
                viewHolder.star.setImageDrawable(null);
            }
    }

    @Override
    public int getItemViewType(int i) {
        return i % 2 * 2;
    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemTitle;
        TextView itemDetail;
        ImageView thumbnail;
        ImageView star;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemTitle = itemView.findViewById(R.id.category_name);
            itemDetail = itemView.findViewById(R.id.itemDetail);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            star = itemView.findViewById(R.id.star);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = RecyclerAdapter.this.itemsList.get((int) getAdapterPosition()).getId();
                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setId(id);
                    Navigation.findNavController(view).navigate(action);
                }
            });
        }
    }
}
