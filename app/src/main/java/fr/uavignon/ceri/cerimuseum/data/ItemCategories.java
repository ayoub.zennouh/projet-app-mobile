package fr.uavignon.ceri.cerimuseum.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "items_categories_table")
public class ItemCategories {

//    public ItemCategories(String itemId, String category) {
//        this.itemId = itemId;
//        this.category = category;
//    }

    public int getId() {
        return id;
    }

    public String getItemId() {
        return itemId;
    }

    public String getCategory() {
        return category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "item_id")
    private String itemId;

    @ColumnInfo(name = "category")
    private String category;
}
