package fr.uavignon.ceri.cerimuseum.data.webservice;

import java.util.List;
import java.util.Map;

public class ItemsResponse {

    public String name = "";
    public List<Integer> timeFrame = null;
    public List<String> categories = null;
    public boolean working = false;
    public int year = 0;
    public List<String> technicalDetails = null;
    public String brand = null;
    public String description = null;
    public Map<String,String> pictures = null;
}
