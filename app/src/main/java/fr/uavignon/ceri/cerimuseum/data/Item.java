package fr.uavignon.ceri.cerimuseum.data;

import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "museum_table")
public class Item {

//    public Item(String id, String name, String description, String categories, String timeFrame) {
//        this.id = id;
//        this.name = name;
//        this.description = description;
//        this.categories = categories;
//        this.timeFrame = timeFrame;
//    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @NonNull
    @ColumnInfo(name="categories")
    private String categories;

    @NonNull
    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    @ColumnInfo(name="year")
    private int year;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name="working")
    private boolean working;

    @ColumnInfo(name="thumbnail")
    private String thumbnail;

    @ColumnInfo(name="liked")
    private boolean liked;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategories() {
        return categories;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public boolean isWorking() {
        return working;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
