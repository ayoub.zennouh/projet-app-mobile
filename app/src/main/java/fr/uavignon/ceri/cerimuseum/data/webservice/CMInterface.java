package fr.uavignon.ceri.cerimuseum.data.webservice;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CMInterface {

    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/collection")
    Call<Map<String, ItemsResponse>> getCollection();

    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/items/{id}")
    Call<ItemsResponse> getItems(@Path("id") String id);

    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/collectionversion")
    Call<Float> getCollectionVersion();

    @Headers("Accept: application/geo+json")
    @POST("/cerimuseum/items/{id}/like")
    Call<String> likeItem(@Path("id") String id);

    @Headers("Accept: application/geo+json")
    @POST("/cerimuseum/items/{id}/dislike")
    Call<String> dislikeItem(@Path("id") String id);

}
