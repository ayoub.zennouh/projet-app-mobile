package fr.uavignon.ceri.cerimuseum.data.database;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Category;
import fr.uavignon.ceri.cerimuseum.data.CollectionVersion;
import fr.uavignon.ceri.cerimuseum.data.FavoriteItems;
import fr.uavignon.ceri.cerimuseum.data.Item;
import fr.uavignon.ceri.cerimuseum.data.ItemCategories;
import fr.uavignon.ceri.cerimuseum.data.ItemPictures;

@Dao
public interface MuseumDao {

    @Insert
    long insert(Item item);

    @Query("DELETE FROM museum_table")
    void deleteAll();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * from museum_table ORDER BY year ASC")
    List<Item> getAllItemsByYear();

    @Query("SELECT * from museum_table ORDER BY year DESC")
    List<Item> getAllItemsByYearDesc();

    @Query("SELECT * from museum_table ORDER BY name DESC")
    List<Item> getSynchrAllItems();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    List<Item> getSynchrAllItemsASC();

    @Query("UPDATE museum_table SET liked = :isLiked WHERE id = :id")
    void updateLike(String id, Boolean isLiked);


    @Query("SELECT * FROM museum_table WHERE id = :id")
    Item getItemById(String id);

    @Query("SELECT * FROM museum_table WHERE id IN (:itemId)")
    LiveData<List<Item>> getItemsByCategory(List<String> itemId);


    @Insert
    long insertVersion(CollectionVersion collectionVersion);

    @Query("SELECT * FROM collection_version_table ORDER BY id DESC limit 1")
    LiveData<CollectionVersion> getLastVersion();

    @Insert
    long insertPicture(ItemPictures itemPictures);

    @Query("SELECT * from items_pictures_table WHERE item_id = :itemId")
    LiveData<List<ItemPictures>> getAllPicturesByItemId(String itemId);

    @Insert
    long insertCategory(ItemCategories itemCategories);

    @Query("SELECT category, count(*) from items_categories_table GROUP BY category")
    LiveData<List<Category>> getAllCategories();

    @Query("SELECT item_id from items_categories_table WHERE category = :category")
    List<String> getItemsIdByCategory(String category);

    @Insert
    long addLike(FavoriteItems favoriteItems);

    @Query("SELECT item_id FROM favorite_items_table")
    List<String> getAllFavorites();

    @Query("DELETE FROM favorite_items_table WHERE item_id = :id")
    void deleteLike(String id);

    @Query("SELECT * FROM favorite_items_table WHERE item_id = :id")
    FavoriteItems isItemLiked(String id);




}
