package fr.uavignon.ceri.cerimuseum;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CategoriesFragment extends Fragment {

    private CategoriesViewModel categoriesViewModel;
    private CategoriesRecyclerAdapter categoriesRecyclerAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        categoriesViewModel = new ViewModelProvider(this).get(CategoriesViewModel.class);


        categoriesViewModel.loadCategories();
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        categoriesRecyclerAdapter = new CategoriesRecyclerAdapter();
        recyclerView.setAdapter(categoriesRecyclerAdapter);

        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {

    }

    private void observerSetup() {
        categoriesViewModel.getAllCategories().observe(getViewLifecycleOwner(), categories ->{
            categoriesRecyclerAdapter.setCategoriesList(categories);
        });
    }

}
