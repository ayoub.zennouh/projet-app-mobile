package fr.uavignon.ceri.cerimuseum.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "items_pictures_table")
public class ItemPictures {

//    public ItemPictures(String itemId, String picture, String caption) {
//        this.itemId = itemId;
//        this.picture = picture;
//        this.caption = caption;
//    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="id")
    private int id;

    @ColumnInfo(name="item_id")
    private String itemId;

    @ColumnInfo(name="picture")
    private String picture;

    @ColumnInfo(name="caption")
    private String caption;

    public int getId() {
        return id;
    }

    public String getItemId() {
        return itemId;
    }

    public String getPicture() {
        return picture;
    }

    public String getCaption() {
        return caption;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
