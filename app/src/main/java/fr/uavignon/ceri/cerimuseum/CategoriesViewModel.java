package fr.uavignon.ceri.cerimuseum;

import android.app.Application;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.cerimuseum.data.Category;
import fr.uavignon.ceri.cerimuseum.data.MuseumRepository;

public class CategoriesViewModel extends AndroidViewModel {

    private MuseumRepository repository;
    private LiveData<List<Category>> allCategories;


    public CategoriesViewModel(@NonNull Application application) {
        super(application);
        repository = MuseumRepository.get(application);
    }

    public void loadCategories(){
        repository.loadCategories();

        allCategories = repository.getAllCategories();
    }

    public LiveData<List<Category>> getAllCategories(){
        return allCategories;
    }
}
